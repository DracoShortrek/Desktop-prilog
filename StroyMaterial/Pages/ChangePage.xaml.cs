﻿using StroyMaterial.DataBase;
using StroyMaterial.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StroyMaterial.Pages
{
    /// <summary>
    /// Логика взаимодействия для ChangePage.xaml
    /// </summary>
    public partial class ChangePage : Page
    {
        Product _product;
        public ChangePage(Product product)
        {
            InitializeComponent();
            _product = product;
            DataContext = _product;
            PageHelper.PageName.Text = "Редактирование товара";

        }

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.GoBack();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите изменить запись?", "Предупреждение", MessageBoxButton.YesNo))
                {
                    PageHelper.ConnectDB.SaveChangesAsync();
                    MessageBox.Show("Данные сохранены");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
