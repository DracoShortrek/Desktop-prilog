﻿using StroyMaterial.DataBase;
using StroyMaterial.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StroyMaterial.Pages
{
    /// <summary>
    /// Логика взаимодействия для ProductList.xaml
    /// </summary>
    public partial class ProductList : Page
    {
        DbSet<Product> product;
        object selectedItem = null;

        public ProductList()
        {

            InitializeComponent();
            PageHelper.PageName.Text = "Список товаров";

            product = PageHelper.ConnectDB.Product;
            UpdateList();

            if (PageHelper.role == 3)
            {

            }
            else
            {
                AddButton.Visibility = Visibility.Collapsed;
                RedactButton.Visibility = Visibility.Collapsed;
                DeleteButton.Visibility = Visibility.Collapsed;
                AccountButton.Visibility = Visibility.Collapsed;
            }
        }

        private void UpdateList()
        {
            product = PageHelper.ConnectDB.Product;
            DataView.ItemsSource = product.ToList();
        }

        private void RedactButton_Click(object sender, RoutedEventArgs e)
        {
            var selected = DataView.SelectedItem as Product;
            if (selected != null)
            {
                PageHelper.MainFrame.Navigate(new ChangePage(selected));
            }
            else
            {
                MessageBox.Show("Нет выбранной записи");
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new AddPage());
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var selected = DataView.SelectedItem as Product;
            if (selected != null)
            {
                if (MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите удалить запись?", "Предупреждение", MessageBoxButton.YesNo))
                {
                    PageHelper.ConnectDB.Product.Remove(selected);
                    PageHelper.ConnectDB.SaveChanges();
                    UpdateList();
                }

            }
            else
            {
                MessageBox.Show("Нет выбранной записи");
            }
        }

        private void AccountButton_Click(object sender, RoutedEventArgs e)
        {
            PageHelper.MainFrame.Navigate(new AccountPage());
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            DataView.ItemsSource = product.Where(x => x.ProductName.Contains(tSearch.Text)).ToList();
        }

        private void filter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tFilter.SelectedItem == F2)
                DataView.ItemsSource = product.Where(x => x.ProductCategory == "Общестроительные материалы").ToList();
            if (tFilter.SelectedItem == F3)
                DataView.ItemsSource = product.Where(x => x.ProductCategory == "Стеновые и фасадные материалы").ToList();
            if (tFilter.SelectedItem == F4)
                DataView.ItemsSource = product.Where(x => x.ProductCategory == "Сухие строительные смеси и гидроизоляция").ToList();
            if (tFilter.SelectedItem == F5)
                DataView.ItemsSource = product.Where(x => x.ProductCategory == "Ручной инструмент").ToList();
            if (tFilter.SelectedItem == F6)
            {
                DataView.ItemsSource = product.Where(x => x.ProductCategory == "Защита лица, глаз, головы").ToList(); e = null;
            }
            
                  
        }

        private void MatView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e != null)
                selectedItem = e.AddedItems[0];
        }

        private void sorting_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sorting.SelectedItem == s2)
                DataView.ItemsSource = product.OrderByDescending(x => x.ProductName).ToList();
            if (sorting.SelectedItem == s3)
                DataView.ItemsSource = product.OrderBy(x => x.ProductName).ToList();

        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateList();
        }
    }
}
